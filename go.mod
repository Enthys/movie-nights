module movie_night

go 1.22.0

require (
	github.com/a-h/templ v0.2.543
	github.com/gorilla/sessions v1.2.2
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/markbates/goth v1.78.0
)

require (
	cloud.google.com/go v0.67.0 // indirect
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2 // indirect
	github.com/gorilla/securecookie v1.1.2 // indirect
	golang.org/x/net v0.17.0 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	google.golang.org/appengine v1.6.6 // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
